# Planet GNOME

Planet GNOME is an aggregator of GNOME Foundation member blogs and GNOME community project blogs.

## This repository

This is a GitLab Pages based implementation of planet.gnome.org.

### Functionality

* Reads feed list from config.ini file (this is a legacy GKeyFile from the old planet-web implementation)
* Uses Python feedparser library to parse the feeds and compose a "Planet GNOME Update"
* Uses Jinja to render the posts in a template
* Uses GitLab CI to publish the result "Planet GNOME Update" html file and assets
