#!/bin/python3

import settings

import dateutil.parser
import configparser
import feedparser
from jinja2 import Environment, FileSystemLoader

LIBRAVATAR_BASE_URL = "https://seccdn.libravatar.org/avatar/"
GRAVATAR_BASE_URL = "https://secure.gravatar.com/avatar/"

class Post:
    author = None
    username = None
    blogurl = None
    photo = None
    title = None
    text = None
    publishing_date = None
    url = None

    def is_valid(self):
        return not (self.publishing_date is None)

    def __init__(self, entry, config):
        try:
            self.publishing_date = entry.published
        except:
            print ("-- No publication date")

        self.author = config.get("name")
        self.username = config.get("nick")
        self.title = entry.get('title')
        self.url = entry.get('link')

        try:
          self.blogurl = entry.get('author_detail').get('href')
        except:
            try:
                self.blogurl = entry.get('source').get('author_detail').get('href')
            except:
                print("-- No author detail")

        self.setup_avatar(config.get("face"))

        # Post content
        if 'content' in entry:
            self.text = entry.content[0].value
        elif 'summary_detail' in entry:
            self.text = entry.summary_detail.value
        else:
            print("-- No post text")

    def setup_avatar(self, face):
        if face is None:
            self.photo = "img/nobody.svg"
        elif face.startswith('libravatar'):
            self.photo = LIBRAVATAR_BASE_URL + face[len('libravatar/'):]
        elif face.startswith('gravatar'):
            self.photo = GRAVATAR_BASE_URL + face[len('gravatar/'):]
        else:
            self.photo = face
            return

        if not self.photo:
            return

        self.photo += "/?s=%s" % settings.AVATAR_IMAGE_SIZE

class FeedCollection:
    _posts = []

    def __init__(self, config_file):
        self._config = self.parse_config_file(config_file)

    def parse_config_file(self, filename):
        config = configparser.ConfigParser()
        config.optionxform = str  # Preserve case sensitivity
        config.read(filename)

        parsed_config = {}
        for section in config.sections():
           section_data = {}
           for key, value in config.items(section):
               section_data[key] = value
           parsed_config[section] = section_data

        return parsed_config

    def fetch_posts(self):
        for url in self._config.keys():
            try:
                feed = feedparser.parse(url,
                                        settings.TITLE)
            except:
                print("%s Indexing Failed: Failed to load feed" % url)
                continue

            self._parse_feed_posts(url, feed)

        self._posts.sort(reverse = True,
                         key=lambda post: dateutil.parser.parse(post.publishing_date))

    def _parse_feed_posts(self, url, feed):
        post_added = False

        num_posts = len(feed['entries'])
        last_posts = feed['entries'][:settings.POSTS_PER_FEED]
        for entry in last_posts:
            post = Post(entry, self._config[url])
            if not post.is_valid():
                print("%s Indexing Failed: Failed to parse post %s" % (url, entry))
                continue

            self._posts.append(post)
            post_added = True

        if (post_added):
            print("%s Indexed" % url)

    def _render_posts(self, posts):
        env = Environment(loader=FileSystemLoader('templates'))
        template = env.get_template('index.html')

        return template.render(site = {
           "title" : settings.TITLE,
           "url" : settings.BASEURL,
        }, posts = posts)

    def publish(self):
        with open('public/index.html', 'w') as html_file:
            html_file.write(self._render_posts(posts = self._posts[:settings.NUMBER_OF_POSTS]))
            print ("Wrote to index.html")

if __name__ == '__main__':
    planet_gnome = FeedCollection("config.ini")
    planet_gnome.fetch_posts()
    planet_gnome.publish()
